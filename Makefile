editor ?= vi
init:
	cp .env.dist .env
	$(editor) .env
	cp application/.env.dist application/.env
	$(editor) application/.env
	docker-compose up --build -d
	docker-compose exec apache make init

update:
	git pull
	docker-compose exec apache make update
