<div align="center">
  <img src="https://upload.wikimedia.org/wikipedia/commons/a/a1/Henri_de_R%C3%A9gnier_by_Vallotton.jpg" height="256">
  <h1>ENCHRE</h1>
  <p>
		<b>Édition Numérique des Cahiers d’Henri de Régnier</b>
	</p>
</div>

## A propos
[univ-grenoble-alpes.fr](http://litt-arts.univ-grenoble-alpes.fr/le-projet-enchre-edition-numerique-des-cahiers-d-henri-de-regnier--345519.kjsp)

[cahier.hypotheses.org](https://cahier.hypotheses.org/enchre)

## Configuration logicielle requise
* git
* make
* docker
* docker-compose

## Installation
```
git clone https://gitlab.com/litt-arts-num/enchre.git
cd enchre
make init
```

## Mise à jour
```
git pull origin master
docker-compose exec apache bash
make update
```

## Commandes (à lancer depuis le container apache)

* toggle le statut admin d'un utilisateur (il lui faudra se déconnecter puis reconnecter)
```
console app:toggle <mail> <role>
console app:toggle test@domain.fr ROLE_ADMIN
```

* vider le cache XSLT/HTML
```
console app:cache:invalidate 
```

* précharger le cache XSLT/HTML
```
console app:cache:warmup 
```

> voir application/Makefile pour d'autres commandes


## Licence
[GNU GENERAL PUBLIC LICENSE V3](LICENSE)

## Crédits
<a href="https://fr.wikipedia.org/wiki/Fichier:Henri_de_R%C3%A9gnier_by_Vallotton.jpg">Portrait of French writer and poet Henri de Régnier (1864-1936) from Le Livre des masques (vol. II, 1898) by Remy de Gourmont (1858-1916) </a>
