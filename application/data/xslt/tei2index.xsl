<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0">

    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>
    <xsl:param name="standalone">0</xsl:param>
    <xsl:param name="common_xml_dir">../../public/upload/common/</xsl:param>
    <xsl:param name="transcriptions_xml_dir">../../public/upload/transcriptions/</xsl:param>
    <xsl:param name="index_type">persons</xsl:param>
    <!--<xsl:param name="index_type">persons|works|notions</xsl:param>-->
    <xsl:param name="show_all" as="xs:boolean">0</xsl:param>
    <!-- 1 if all occurrences must be displayed even if no @corresp in the content -->
    <xsl:include href="common.xsl"/>

    <xsl:template match="/">
        <xsl:variable name="common_collection" select="concat($common_xml_dir, '?select=*.xml')"/>
        <xsl:variable name="tei_common_files" select="collection($common_collection)"/>
        <xsl:variable name="entries"
            select="$tei_common_files/tei:TEI/tei:text/tei:body//tei:div[@xml:id = 'formesnormalisées']"/>
        <xsl:variable name="transcriptions_collection"
            select="concat($transcriptions_xml_dir, '?select=*-split.xml')"/>
        <xsl:variable name="tei_transcriptions_files"
            select="collection($transcriptions_collection)"/>

        <xsl:variable name="letters">
            <xsl:text>ABCDEFGHIJKLMNOPQRSTUVWXYZ</xsl:text>
        </xsl:variable>
        <xsl:for-each select="tokenize($letters, '.')">
            <xsl:if test="position() &lt; last()">
                <xsl:variable name="letter" select="substring($letters, position(), 1)"/>
                <div class="mt-2 letter-block">
                    <div class="card">
                        <div id="index-{$letter}" class="card-header">
                            <xsl:value-of select="$letter"/>
                        </div>
                        <ul class="list-group list-group-flush text-justify">
                            <xsl:choose>
                                <xsl:when test="$index_type = 'persons'">
                                    <!-- [1] : if several tei:div[@xml:id = 'formesnormalisées'], just use the first one -->
                                    <xsl:apply-templates select="$entries[1]" mode="person_index">
                                        <xsl:with-param name="letter" select="$letter"/>
                                        <xsl:with-param name="tei_transcriptions_files"
                                            select="$tei_transcriptions_files"/>
                                        <xsl:with-param name="show_all" as="xs:boolean"
                                            select="$show_all"/>
                                    </xsl:apply-templates>
                                </xsl:when>
                                <xsl:when test="$index_type = 'works'">
                                    <!-- [1] : if several tei:div[@xml:id = 'formesnormalisées'], just use the first one -->
                                    <xsl:apply-templates select="$entries[1]" mode="works_index">
                                        <xsl:with-param name="letter" select="$letter"/>
                                        <xsl:with-param name="tei_transcriptions_files"
                                            select="$tei_transcriptions_files"/>
                                        <xsl:with-param name="show_all" as="xs:boolean"
                                            select="$show_all"/>
                                    </xsl:apply-templates>
                                </xsl:when>
                                <xsl:when test="$index_type = 'notions'">
                                    <!-- [1] : if several tei:div[@xml:id = 'formesnormalisées'], just use the first one -->
                                    <xsl:apply-templates select="$entries[1]" mode="notions_index">
                                        <xsl:with-param name="letter" select="$letter"/>
                                        <xsl:with-param name="tei_transcriptions_files"
                                            select="$tei_transcriptions_files"/>
                                        <xsl:with-param name="show_all" as="xs:boolean"
                                            select="$show_all"/>
                                    </xsl:apply-templates>
                                </xsl:when>
                                <xsl:otherwise>
                                    <p>Index en construction</p>
                                </xsl:otherwise>
                            </xsl:choose>
                        </ul>
                    </div>
                </div>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="tei:div" mode="person_index">
        <xsl:param name="letter" required="yes"/>
        <xsl:param name="tei_transcriptions_files" required="yes"/>
        <xsl:param name="show_all" as="xs:boolean">0</xsl:param>

        <xsl:variable name="person_starting_with_current_letter"
            select=".//tei:listPerson/tei:person[substring(@xml:id, 1, 1) = lower-case($letter)] | .//tei:listPerson/tei:person//tei:addName[matches(normalize-space(.), concat('^', lower-case($letter)), 'i')]"/>
        <xsl:for-each select="$person_starting_with_current_letter">
            <xsl:sort select="@xml:id or normalize-space(.)"/>
            <xsl:variable name="person_id">
                <xsl:choose>
                    <xsl:when test="name() = 'addName'">
                        <xsl:value-of select="ancestor::tei:person[@xml:id]/@xml:id"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="@xml:id"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <!--<xsl:if test="
                $show_all or
                exists($tei_transcriptions_files/tei:TEI[tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/tei:name/@type = 'cahier'][exists(tei:text/tei:body//@ref[. = concat('#', $person_id)][exists(ancestor::tei:div[@type = 'page']/tei:fw[@type = 'num_page_ref'])])])">-->
            <xsl:variable name="div_list"
                select="$tei_transcriptions_files/tei:TEI[tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/tei:name/@type = 'cahier']/tei:text/tei:body//tei:div[@type = 'page'][tei:fw[@type][@type = 'num_page_ref']]"/>
            <xsl:if test="
                    $show_all or
                    exists($div_list//*[@ref][@ref = concat('#', $person_id)])">
                <li id="{@xml:id}" class="notice-entry list-group-item">
                    <b>
                        <xsl:apply-templates select="." mode="index"/>
                    </b>
                    <xsl:text> </xsl:text>
                    <xsl:choose>
                        <xsl:when test="name() = 'addName'">
                            <p>
                                <small>
                                    <i>
                                        <xsl:text>Voir : </xsl:text>
                                        <a href="#{$person_id}">
                                            <xsl:apply-templates
                                                select="ancestor::tei:person[@xml:id]" mode="index"
                                            />
                                        </a>
                                    </i>
                                </small>
                            </p>
                        </xsl:when>
                        <xsl:otherwise>
                            <!-- 3e test : un peu limitatif mais plus robuste : on n'affiche une occurrence que si l'on trouve à l'endroit attendu le numéro de feuillet concerné.
                                            Bref, s'il en manque, c'est possiblement que l'xml:id n'est pas correct mais aussi que la structure TEI n'est pas celle attendue... -->
                            <xsl:for-each-group select="
                                    $tei_transcriptions_files/tei:TEI[tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/tei:name/@type = 'cahier'
                                    and exists(tei:text/tei:body//*[@ref = concat('#', $person_id) and exists(ancestor::tei:div[@type = 'page']/tei:fw[@type = 'num_page_ref'])])]"
                                group-by="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/tei:name/@xml:id">
                                <xsl:sort
                                    select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/tei:name/@xml:id"
                                    data-type="number" order="ascending"/>
                                <xsl:variable name="cahier_id" select="current-grouping-key()"/>
                                <xsl:if test="position() = 1">
                                    <xsl:text disable-output-escaping="yes">&lt;ul></xsl:text>
                                </xsl:if>
                                <li>
                                    <span data-type="cahier">
                                        <xsl:value-of
                                            select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/tei:name"
                                        />
                                    </span>
                                    <xsl:text> :</xsl:text>
                                    <xsl:for-each-group
                                        select="tei:text/tei:body//*[@ref = concat('#', $person_id) and exists(ancestor::tei:div[@type = 'page']/tei:fw[@type = 'num_page_ref'])]"
                                        group-by="ancestor::tei:div[@type = 'page']/tei:fw[@type = 'num_page_ref']">
                                        <xsl:text> </xsl:text>
                                        <xsl:variable name="folio_id"
                                            select="ancestor::tei:div[@type = 'page']/tei:fw[@type = 'num_page_ref']"/>
                                        <a href="/display/{$cahier_id}/{$folio_id}#{$person_id}">
                                            <xsl:value-of select="$folio_id"/>
                                        </a>
                                    </xsl:for-each-group>
                                </li>
                                <xsl:if test="position() = last()">
                                    <xsl:text disable-output-escaping="yes">&lt;/ul></xsl:text>
                                </xsl:if>
                            </xsl:for-each-group>
                        </xsl:otherwise>
                    </xsl:choose>
                </li>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="tei:div" mode="works_index">
        <xsl:param name="letter" required="yes"/>
        <xsl:param name="tei_transcriptions_files" required="yes"/>
        <xsl:param name="show_all" as="xs:boolean">0</xsl:param>

        <xsl:variable name="bibl_starting_with_current_letter"
            select=".//tei:listBibl/tei:bibl[substring(@xml:id, 1, 1) = lower-case($letter)]"/>
        <xsl:for-each select="$bibl_starting_with_current_letter">
            <xsl:sort select="@xml:id"/>
            <xsl:variable name="bibl_id" select="@xml:id"/>
            <!--[<xsl:value-of select="$bibl_id"/>]-->
            <xsl:variable name="occurrence" select="
                    $tei_transcriptions_files/tei:TEI[tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/tei:name/@type = 'cahier'
                    and exists(tei:text/tei:body//*[@ref = concat('#', $bibl_id) and exists(ancestor::tei:div[@type = 'page']/tei:fw[@type = 'num_page_ref'])])]"/>
            <xsl:if test="$show_all or count($occurrence) > 0">
                <li id="{@xml:id}" class="notice-entry list-group-item">
                    <b>
                        <xsl:apply-templates select="." mode="index"/>
                    </b>
                    <xsl:text> </xsl:text>
                    <!-- 3e test : un peu limitatif mais plus robuste : on n'affiche une occurrence que si l'on trouve à l'endroit attendu le numéro de feuillet concerné.
                                            Bref, s'il en manque, c'est possiblement que l'xml:id n'est pas correct mais aussi que la structure TEI n'est pas celle attendue... -->
                    <xsl:for-each-group select="$occurrence"
                        group-by="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/tei:name/@xml:id">
                        <xsl:sort
                            select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/tei:name/@xml:id"
                            data-type="number" order="ascending"/>
                        <xsl:variable name="cahier_id" select="current-grouping-key()"/>
                        <xsl:if test="position() = 1">
                            <xsl:text disable-output-escaping="yes">&lt;ul></xsl:text>
                        </xsl:if>
                        <li>
                            <span data-type="cahier">
                                <xsl:value-of
                                    select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/tei:name"
                                />
                            </span>
                            <xsl:text> :</xsl:text>
                            <xsl:for-each-group
                                select="tei:text/tei:body//*[@ref = concat('#', $bibl_id) and exists(ancestor::tei:div[@type = 'page']/tei:fw[@type = 'num_page_ref'])]"
                                group-by="ancestor::tei:div[@type = 'page']/tei:fw[@type = 'num_page_ref']">
                                <xsl:text> </xsl:text>
                                <xsl:variable name="folio_id"
                                    select="ancestor::tei:div[@type = 'page']/tei:fw[@type = 'num_page_ref']"/>
                                <a href="/display/{$cahier_id}/{$folio_id}#{$bibl_id}">
                                    <xsl:value-of select="$folio_id"/>
                                </a>
                            </xsl:for-each-group>
                        </li>
                        <xsl:if test="position() = last()">
                            <xsl:text disable-output-escaping="yes">&lt;/ul></xsl:text>
                        </xsl:if>
                    </xsl:for-each-group>
                </li>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="tei:div" mode="notions_index">
        <xsl:param name="letter" required="yes"/>
        <xsl:param name="tei_transcriptions_files" required="yes"/>
        <xsl:param name="show_all" as="xs:boolean">0</xsl:param>

        <xsl:variable name="item_starting_with_current_letter"
            select=".//tei:list/tei:item[lower-case(substring(@xml:id, 1, 1)) = lower-case($letter)]"/>
        <xsl:for-each select="$item_starting_with_current_letter">
            <xsl:sort select="@xml:id" lang="fr" order="ascending" data-type="text"/>
            <xsl:variable name="entry_id" select="@xml:id"/>
            <xsl:if test="
                    $show_all or (count($tei_transcriptions_files/tei:TEI[tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/tei:name/@type = 'cahier'
                    and exists(tei:text/tei:body//tei:seg[@ana = concat('#', $entry_id) and exists(ancestor::tei:div[@type = 'page']/tei:fw[@type = 'num_page_ref'])])]) > 0)">
                <li id="{@xml:id}" class="notice-entry list-group-item">
                    <b>
                        <xsl:value-of select="."/>
                    </b>
                    <!-- 3e test : un peu limitatif mais plus robuste : on n'affiche une occurrence que si l'on trouve à l'endroit attendu le numéro de feuillet concerné.
                                            Bref, s'il en manque, c'est possiblement que l'xml:id n'est pas correct mais aussi que la structure TEI n'est pas celle attendue... -->
                    <xsl:for-each-group select="
                            $tei_transcriptions_files/tei:TEI[tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/tei:name/@type = 'cahier'
                            and exists(tei:text/tei:body//tei:seg[@ana = concat('#', $entry_id) and exists(ancestor::tei:div[@type = 'page']/tei:fw[@type = 'num_page_ref'])])]"
                        group-by="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/tei:name/@xml:id">
                        <xsl:sort
                            select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/tei:name/@xml:id"
                            data-type="number" order="ascending"/>
                        <xsl:variable name="cahier_id" select="current-grouping-key()"/>
                        <xsl:if test="position() = 1">
                            <xsl:text disable-output-escaping="yes">&lt;ul></xsl:text>
                        </xsl:if>
                        <li>
                            <span data-type="cahier">
                                <xsl:value-of
                                    select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/tei:name"
                                />
                            </span>
                            <xsl:text> :</xsl:text>
                            <xsl:for-each-group
                                select="tei:text/tei:body//tei:seg[@ana = concat('#', $entry_id) and exists(ancestor::tei:div[@type = 'page']/tei:fw[@type = 'num_page_ref'])]"
                                group-by="ancestor::tei:div[@type = 'page']/tei:fw[@type = 'num_page_ref']">
                                <xsl:text> </xsl:text>
                                <xsl:variable name="folio_id"
                                    select="ancestor::tei:div[@type = 'page']/tei:fw[@type = 'num_page_ref']"/>
                                <a href="/display/{$cahier_id}/{$folio_id}#{$entry_id}">
                                    <xsl:value-of select="$folio_id"/>
                                </a>
                            </xsl:for-each-group>
                        </li>
                        <xsl:if test="position() = last()">
                            <xsl:text disable-output-escaping="yes">&lt;/ul></xsl:text>
                        </xsl:if>
                    </xsl:for-each-group>
                </li>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="tei:form">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="tei:sense">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="tei:title">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>

    <xsl:template match="tei:hi[@rend = 'exp']">
        <sup>
            <xsl:apply-templates/>
        </sup>
    </xsl:template>

    <xsl:template match="tei:lb">
        <br/>
    </xsl:template>

    <xsl:template match="tei:foreign">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>

    <xsl:template match="tei:ref[@target]">
        <a href="{@target}">
            <xsl:apply-templates/>
        </a>
    </xsl:template>

    <xsl:template match="tei:bibl" mode="index">
        <xsl:choose>
            <xsl:when
                test="count(child::*) &gt;= 6 and tei:author and tei:title[@level = 's'] and tei:publisher and tei:date and tei:editor and tei:biblScope">
                <xsl:for-each select="tei:author">
                    <xsl:apply-templates select="." mode="index"/>
                    <xsl:if test="position() &gt;= 1 and position() &lt; last() - 1">
                        <xsl:text>, </xsl:text>
                    </xsl:if>
                    <xsl:if test="position() &gt;= 1 and position() = last() - 1">
                        <xsl:text> et </xsl:text>
                    </xsl:if>
                </xsl:for-each>
                <xsl:text>, </xsl:text>
                <i>
                    <xsl:value-of select="tei:title[@level = 's']"/>
                </i>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:publisher"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:biblScope"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:date"/>
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="
                    count(child::*) &gt;= 5 and
                    count(tei:author) &gt;= 1 and
                    tei:title[@level = 'a'] and
                    tei:title[@level = 'j'] and
                    tei:biblScope and
                    tei:date">
                <xsl:for-each select="tei:author">
                    <xsl:apply-templates select="." mode="index"/>
                    <xsl:if test="position() &gt;= 1 and position() &lt; last() - 1">
                        <xsl:text>, </xsl:text>
                    </xsl:if>
                    <xsl:if test="position() &gt;= 1 and position() = last() - 1">
                        <xsl:text> et </xsl:text>
                    </xsl:if>
                </xsl:for-each>
                <xsl:text>, </xsl:text>
                <i>
                    <xsl:value-of select="tei:title[@level = 'a']"/>
                </i>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:title[@level = 'j']"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:date"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:biblScope"/>
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="
                    count(child::*) &gt;= 4 and
                    count(tei:author) &gt;= 1 and
                    tei:title[@level = 'm'] and
                    tei:publisher and
                    tei:date">
                <xsl:for-each select="tei:author">
                    <xsl:apply-templates select="." mode="index"/>
                    <xsl:if test="position() &gt;= 1 and position() &lt; last() - 1">
                        <xsl:text>, </xsl:text>
                    </xsl:if>
                    <xsl:if test="position() &gt;= 1 and position() = last() - 1">
                        <xsl:text> et </xsl:text>
                    </xsl:if>
                </xsl:for-each>
                <xsl:text>, </xsl:text>
                <i>
                    <xsl:value-of select="tei:title[@level = 'm']"/>
                </i>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:publisher"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:date"/>
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when
                test="count(child::*) = 4 and tei:author and tei:title[@level = 'm'] and tei:title[@level = 'j'] and tei:date">
                <xsl:apply-templates select="tei:author" mode="index"/>
                <xsl:text>, </xsl:text>
                <i>
                    <xsl:value-of select="tei:title[@level = 'm']"/>
                </i>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:title[@level = 'j']"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:date"/>
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when
                test="count(child::*) = 4 and tei:author and tei:title[@level = 'a'] and tei:title[@level = 'j'] and tei:date">
                <xsl:apply-templates select="tei:author" mode="index"/>
                <xsl:text>, </xsl:text>
                <i>
                    <xsl:value-of select="tei:title[@level = 'a']"/>
                </i>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:title[@level = 'j']"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:date"/>
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when
                test="count(child::*) = 4 and tei:author and tei:title[@level = 'm'] and tei:publisher and tei:date">
                <xsl:apply-templates select="tei:author" mode="index"/>
                <xsl:text>, </xsl:text>
                <i>
                    <xsl:value-of select="tei:title[@level = 'm']"/>
                </i>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:publisher"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:date"/>
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when
                test="count(child::*) = 4 and tei:editor and tei:title[@level = 'm'] and tei:publisher and tei:date">
                <i>
                    <xsl:value-of select="tei:title[@level = 'm']"/>
                </i>
                <xsl:text>, </xsl:text>
                <xsl:apply-templates select="tei:editor" mode="index"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:publisher"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:date"/>
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when
                test="count(child::*) = 3 and tei:title[@level = 'j'] and tei:editor and tei:date">
                <xsl:value-of select="tei:title[@level = 'j']"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:editor"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:date"/>
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when
                test="count(child::*) = 3 and tei:title[@level = 'm'] and tei:publisher and tei:date">
                <xsl:value-of select="tei:title[@level = 'm']"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:publisher"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:date"/>
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when
                test="count(child::*) = 3 and tei:title[@level = 'm'] and tei:author and tei:date">
                <xsl:value-of select="tei:author"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:title[@level = 'm']"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:date"/>
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="count(child::*) = 2 and tei:title[@level = 'j'] and tei:date">
                <xsl:value-of select="tei:title[@level = 'j']"/>
                <xsl:text>, </xsl:text>
                <xsl:value-of select="tei:date"/>
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:when test="count(child::*) = 1 and tei:title[@level = 'j']">
                <xsl:value-of select="tei:title[@level = 'j']"/>
                <xsl:text>.</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <span class="text-warning">
                    <xsl:text>[todo </xsl:text>
                    <xsl:value-of select="count(*)"/>
                    <xsl:text>] </xsl:text>
                    <xsl:for-each select="*">
                        <xsl:text>&lt;</xsl:text>
                        <xsl:value-of select="name()"/>
                        <xsl:for-each select="@*">
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="name()"/>
                            <xsl:text>="</xsl:text>
                            <xsl:value-of select="."/>
                            <xsl:text>"</xsl:text>
                        </xsl:for-each>
                        <xsl:text>&gt;</xsl:text>
                        <xsl:value-of select="."/>
                        <xsl:text>&lt;/</xsl:text>
                        <xsl:value-of select="name()"/>
                        <xsl:text>&gt;</xsl:text>
                    </xsl:for-each>
                </span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:author" mode="index">
        <xsl:choose>
            <xsl:when test="count(child::*) = 2 and tei:surname and tei:forename">
                <xsl:value-of select="tei:forename"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="tei:surname"/>
            </xsl:when>
            <xsl:when test="count(child::*) = 3 and tei:surname and tei:forename and tei:nameLink">
                <xsl:value-of select="tei:forename"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="tei:nameLink"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="tei:surname"/>
            </xsl:when>
            <xsl:when test="count(child::*) = 1 and tei:surname">
                <xsl:value-of select="tei:surname"/>
            </xsl:when>
            <xsl:otherwise>
                <span class="text-warning">
                    <xsl:text>[todo] </xsl:text>
                    <xsl:for-each select="*">
                        <xsl:text>&lt;</xsl:text>
                        <xsl:value-of select="name()"/>
                        <!--<xsl:for-each select="@*">
                <xsl:text> </xsl:text>
                <xsl:value-of select="name()"/>
                <xsl:text>="</xsl:text>
                <xsl:value-of select="."/>
                <xsl:text>"</xsl:text>
            </xsl:for-each>-->
                        <xsl:text>&gt;</xsl:text>
                        <xsl:value-of select="."/>
                        <xsl:text>&gt;/</xsl:text>
                        <xsl:value-of select="name()"/>
                        <xsl:text>&gt;</xsl:text>
                    </xsl:for-each>
                </span>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="*">
        <button class="btn btn-sm bg-warning m-1 p-1">
            <xsl:variable name="message">
                <xsl:value-of select="name(.)"/>
                <xsl:for-each select="@*">
                    <xsl:choose>
                        <xsl:when test="position() = 1">
                            <xsl:text>(</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text> </xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                    <xsl:text>@</xsl:text>
                    <xsl:value-of select="name(.)"/>
                    <xsl:text>=</xsl:text>
                    <xsl:value-of select="."/>
                    <xsl:if test="position() = last()">
                        <xsl:text>)</xsl:text>
                    </xsl:if>
                </xsl:for-each>
            </xsl:variable>
            <xsl:value-of select="$message"/>
            <xsl:if test="$message = '1'">
                <xsl:message select="$message"/>
            </xsl:if>
        </button>
    </xsl:template>

</xsl:stylesheet>
