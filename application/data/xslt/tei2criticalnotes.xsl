<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mon-document [
    <!ENTITY times "&#215;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:str="http://exslt.org/strings" exclude-result-prefixes="xs tei str" version="1.0">
    <xsl:import href="str.tokenize.template.xsl"/>
    <!--<xsl:output method="html" omit-xml-declaration="yes" indent="no"/>-->
    <!--<xsl:strip-space elements="*"/>-->

    <xsl:template match="/tei:TEI" mode="apparatus">
        <xsl:for-each select="tei:text/tei:body//tei:note">
            <xsl:apply-templates mode="notes"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="tei:note" mode="notes">
        <dt>
            <xsl:attribute name="data-note-title-id">
                <xsl:text>#n</xsl:text>
                <xsl:number count="tei:note" level="any"/>
            </xsl:attribute>
            <xsl:number count="tei:note" level="any"/>
        </dt>
        <dd data-hand="{@resp}">
            <xsl:attribute name="data-note-content-id">
                <xsl:text>#n</xsl:text>
                <xsl:number count="tei:note" level="any"/>
            </xsl:attribute>
            <xsl:apply-templates mode="apparatus"/>
            <xsl:if test="@resp">
                <xsl:text> [</xsl:text>
                <xsl:value-of select="@resp"/>
                <xsl:text>]</xsl:text>
            </xsl:if>
        </dd>
    </xsl:template>


    <!-- OLD -->
    <!--<xsl:template match="tei:div/*[not(self::tei:note)]" mode="apparatus"/>-->
    <xsl:template match="tei:div" mode="apparatus">
        <xsl:variable name="note_list">
            <xsl:call-template name="note_list"/>
        </xsl:variable>
        <xsl:for-each select="exsl:node-set($note_list)/*" xmlns:exsl="http://exslt.org/common">
            <xsl:choose>
                <xsl:when test="@type = 'err:not_available'">
                    <dt data-note-title-id="{name()}">
                        <xsl:value-of select="@num"/>
                    </dt>
                    <dd data-note-content-id="{name()}" data-hand="unknown">
                        <i title="La note {name()} n'est pas accessible">Note indisponible</i>.
                    </dd>
                </xsl:when>
                <xsl:when test="@type = 'err:more_than_one_target'">
                    <dt data-note-title-id="{name()}">
                        <xsl:value-of select="@num"/>
                    </dt>
                    <dd data-note-content-id="{name()}" data-hand="unknown">
                        <i title="Il existe plusieurs note portant l'identifiant {name()}.">Note
                            indisponible</i>. </dd>
                </xsl:when>
                <xsl:when test="@type = 'ann'">
                    <xsl:apply-templates mode="apparatus" select="./tei:note">
                        <xsl:with-param name="note_num" select="@num"/>
                    </xsl:apply-templates>
                </xsl:when>
                <xsl:when test="@type = 'not'">
                    <xsl:apply-templates mode="biographicnote" select="./tei:note">
                        <xsl:with-param name="note_num" select="@num"/>
                    </xsl:apply-templates>
                </xsl:when>
                <xsl:otherwise>
                    <!--<xsl:value-of select="substring(//tei:note[@xml:id = $note_id]/@ana, 1, 4)"/>-->
                    <xsl:apply-templates mode="unknownnote" select="./tei:note">
                        <xsl:with-param name="note_num" select="@num"/>
                    </xsl:apply-templates>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="tei:div" mode="apparatus_old">
        <xsl:for-each select=".//tei:ref">
            <xsl:variable name="note_id">
                <xsl:choose>
                    <xsl:when
                        test="string-length(translate(normalize-space(@target), 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-#', '')) > 0">
                        <xsl:value-of select="substring-before(substring-after(@target, '#'), ' ')"
                        />
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="substring-after(@target, '#')"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="note_type">
                <xsl:choose>
                    <xsl:when test="count(//tei:note[@xml:id = $note_id]) = 0">
                        <xsl:text>err:not_available</xsl:text>
                    </xsl:when>
                    <xsl:when test="count(//tei:note[@xml:id = $note_id]) > 1">
                        <xsl:text>err:more_than_one_target</xsl:text>
                    </xsl:when>
                    <xsl:when test="not(//tei:note[@xml:id = $note_id]/@ana)">
                        <xsl:text>err:missing_ann_attribute</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="substring(//tei:note[@xml:id = $note_id]/@ana, 2, 3)"
                        />
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="note_count">
                <xsl:value-of
                    select="count(preceding::tei:ref[ancestor::tei:div[@type = 'page'] = current()/ancestor::tei:div[@type = 'page']]) + 1"
                />
            </xsl:variable>
            <xsl:variable name="prev_ann_note">
                <xsl:for-each
                    select="preceding::tei:ref[ancestor::tei:div[@type = 'page'] = current()/ancestor::tei:div[@type = 'page']]">
                    <xsl:variable name="this_note_id">
                        <xsl:choose>
                            <xsl:when
                                test="string-length(translate(normalize-space(@target), 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-#', '')) > 0">
                                <xsl:value-of
                                    select="substring-before(substring-after(@target, '#'), ' ')"/>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="substring-after(@target, '#')"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:if test="substring(//tei:note[@xml:id = $this_note_id]/@ana, 2, 3) = 'ann'">
                        <xsl:text>1</xsl:text>
                    </xsl:if>
                </xsl:for-each>
            </xsl:variable>
            <xsl:variable name="note_num">
                <xsl:value-of select="string-length($prev_ann_note) + 1"/>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="count(//tei:note[@xml:id = $note_id]) = 0">
                    <dt data-note-title-id="{@target}">
                        <xsl:value-of select="$note_num"/>
                    </dt>
                    <dd data-note-content-id="{@target}" data-hand="unknown">
                        <i title="La note {@target} n'est pas accessible">Note indisponible</i>.
                    </dd>
                </xsl:when>
                <xsl:when test="count(//tei:note[@xml:id = $note_id]) > 1">
                    <dt data-note-title-id="{@target}">
                        <xsl:value-of select="$note_num"/>
                    </dt>
                    <dd data-note-content-id="{@target}" data-hand="unknown">
                        <i title="Il existe plusieurs note portant l'identifiant {@target}.">Note
                            indisponible</i>. </dd>
                </xsl:when>
                <xsl:when test="substring(//tei:note[@xml:id = $note_id]/@ana, 1, 4) = '#ann'">
                    <xsl:apply-templates mode="apparatus" select="//tei:note[@xml:id = $note_id]">
                        <xsl:with-param name="note_num" select="$note_num"/>
                    </xsl:apply-templates>
                </xsl:when>
                <xsl:when test="substring(//tei:note[@xml:id = $note_id]/@ana, 1, 4) = '#not'">
                    <xsl:apply-templates mode="biographicnote"
                        select="//tei:note[@xml:id = $note_id]">
                        <xsl:with-param name="note_num" select="$note_num"/>
                    </xsl:apply-templates>
                </xsl:when>
                <xsl:otherwise>
                    <!--<xsl:value-of select="substring(//tei:note[@xml:id = $note_id]/@ana, 1, 4)"/>-->
                    <xsl:apply-templates mode="unknownnote" select="//tei:note[@xml:id = $note_id]">
                        <xsl:with-param name="note_num" select="$note_num"/>
                    </xsl:apply-templates>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>

    <xsl:template name="annotations" match="tei:note" mode="apparatus">
        <xsl:param name="note_num"/>
        <dt>
            <xsl:attribute name="data-note-title-id">
                <xsl:value-of select="@xml:id"/>
            </xsl:attribute>
            <xsl:value-of select="$note_num"/>
        </dt>
        <dd>
            <xsl:attribute name="data-note-content-id">
                <xsl:value-of select="@xml:id"/>
            </xsl:attribute>
            <xsl:attribute name="data-hand">
                <xsl:value-of select="@hand | @resp"/>
            </xsl:attribute>
            <xsl:apply-templates mode="apparatus"/> [<xsl:value-of
                select="translate(translate(@resp, '#', ''), ' ', ',')"/>] </dd>
    </xsl:template>
    <xsl:template name="notes" match="tei:note" mode="biographicnote">
        <xsl:param name="note_num"/>
        <div style="display:none">
            <dt>
                <xsl:attribute name="data-note-title-id">
                    <xsl:value-of select="@xml:id"/>
                </xsl:attribute> Notice biographique </dt>
            <dd>
                <xsl:attribute name="data-note-content-id">
                    <xsl:value-of select="@xml:id"/>
                </xsl:attribute>
                <xsl:attribute name="data-hand">
                    <xsl:value-of select="@hand | @resp"/>
                </xsl:attribute>
                <xsl:apply-templates mode="apparatus"/> [<xsl:value-of
                    select="translate(translate(@resp, '#', ''), ' ', ',')"/>] </dd>
        </div>
    </xsl:template>
    <xsl:template name="other" match="tei:note" mode="unknownnote">
        <xsl:param name="note_num"/>
        <div style="font-size: small; display: none;">
            <dt>
                <xsl:attribute name="data-note-title-id">
                    <xsl:value-of select="@xml:id"/>
                </xsl:attribute> Note non classée</dt>
            <dd>
                <xsl:attribute name="data-note-content-id">
                    <xsl:value-of select="@xml:id"/>
                </xsl:attribute>
                <xsl:attribute name="data-hand">
                    <xsl:value-of select="@hand | @resp"/>
                </xsl:attribute>
                <sup style="float:right;"
                    title="Type de note non ou mal déclaré. Renseigner ou corriger l'attribut @ana de la note @xml:id='{@xml:id}'"
                    >Pourquoi cette note est dite "non classée" ?</sup>
                <xsl:apply-templates mode="apparatus"/> [<xsl:value-of
                    select="translate(translate(@resp, '#', ''), ' ', ',')"/>] </dd>
        </div>
    </xsl:template>

    <xsl:template match="tei:note/child::*[1][name() = 'hi']" mode="apparatus"/>
    <xsl:template match="tei:p" mode="apparatus">
        <p>
            <xsl:apply-templates mode="apparatus"/>
        </p>
    </xsl:template>
    <xsl:template match="tei:title" mode="apparatus">
        <i>
            <xsl:apply-templates mode="apparatus"/>
        </i>
    </xsl:template>
    <xsl:template match="tei:persName" mode="apparatus">
        <mark class="enrichment" title="Nom de personne">
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates mode="apparatus"/>
        </mark>
    </xsl:template>
    <xsl:template match="tei:placeName" mode="apparatus">
        <mark class="enrichment" title="Nom de lieu">
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates mode="apparatus"/>
        </mark>
    </xsl:template>
    <xsl:template match="tei:ref" mode="apparatus">
        <xsl:apply-templates mode="apparatus"/>
        <a href="{@target}">
            <i class="fa fa-link"/>
        </a>
    </xsl:template>
    <xsl:template match="tei:lb" mode="apparatus">
        <br/>
    </xsl:template>
    <xsl:template match="tei:foreign" mode="apparatus">
        <mark class="enrichment" title="Emprunt">
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates mode="apparatus"/>
        </mark>
    </xsl:template>
    <xsl:template match="tei:span" mode="apparatus">
        <span title="Utilisation de la balise &lt;span/&gt;" style="border: 1px dashed red">

            <xsl:copy-of select="@*"/>
            <xsl:apply-templates mode="apparatus"/>
            <i class="fa fa-alert"/>
        </span>
    </xsl:template>
    <xsl:template match="tei:graphic" mode="apparatus">
        <img width="{@width}" src="{@url}"/>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'exp']" mode="apparatus">
        <sup>
            <xsl:apply-templates mode="apparatus"/>
        </sup>
    </xsl:template>
    <xsl:template match="tei:hi[@rend = 'italic']" mode="apparatus">
        <i>
            <xsl:apply-templates mode="apparatus"/>
        </i>
    </xsl:template>
    <xsl:template match="tei:hi" mode="apparatus">
        <hi style="background-color: red">
            <xsl:copy-of select="@*"/>
            <xsl:apply-templates mode="apparatus"/>
        </hi>
    </xsl:template>
    <xsl:template match="tei:lg" mode="apparatus">
        <div class="poem">
            <xsl:apply-templates mode="apparatus"/>
        </div>
    </xsl:template>
    <xsl:template match="tei:l" mode="apparatus">
        <span class="poem_line">
            <xsl:apply-templates mode="apparatus"/>
        </span>
    </xsl:template>
    <xsl:template match="node()" mode="apparatus">
        <xsl:if test="name() = ''">
            <xsl:value-of select="."/>
        </xsl:if>
        <xsl:if test="not(name() = '')">
            <xsl:value-of select="."/>
            <i class="fa fa-alert" title="Élément tei '{name()}' non traité par tei2criticalnotes"/>
        </xsl:if>
    </xsl:template>


    <xsl:template name="note_list">
        <xsl:variable name="all_list">
            <xsl:for-each select=".//tei:ref">
                <xsl:variable name="sep">
                    <xsl:text> </xsl:text>
                </xsl:variable>
                <xsl:variable name="ids">
                    <xsl:call-template name="str:tokenize">
                        <xsl:with-param name="string" select="@target"/>
                        <xsl:with-param name="delimiters" select="$sep"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:variable name="notes">
                    <xsl:copy-of select="//tei:note"/>
                </xsl:variable>
                <xsl:for-each select="exsl:node-set($ids)/token"
                    xmlns:exsl="http://exslt.org/common">
                    <xsl:variable name="id">
                        <xsl:value-of select="substring-after(., '#')"/>
                    </xsl:variable>
                    <!--<xsl:message>[<xsl:value-of select="count($notes/tei:note[@xml:id = 'HR_1887-02_PL_72'])"/>]
                    [<xsl:value-of select="$id"/>]</xsl:message>-->
                    <xsl:variable name="type">
                        <xsl:choose>
                            <xsl:when
                                test="count(exsl:node-set($notes)/tei:note[@xml:id = $id]) = 0"
                                xmlns:exsl="http://exslt.org/common">
                                <xsl:text>err:not_available</xsl:text>
                            </xsl:when>
                            <xsl:when
                                test="count(exsl:node-set($notes)/tei:note[@xml:id = $id]) > 1"
                                xmlns:exsl="http://exslt.org/common">
                                <xsl:text>err:more_than_one_target</xsl:text>
                            </xsl:when>
                            <xsl:when test="not(exsl:node-set($notes)/tei:note[@xml:id = $id]/@ana)"
                                xmlns:exsl="http://exslt.org/common">
                                <xsl:text>err:missing_ann_attribute</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of
                                    select="substring(exsl:node-set($notes)/tei:note[@xml:id = $id]/@ana, 2, 3)"
                                    xmlns:exsl="http://exslt.org/common"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:element name="azerty{$id}">
                        <xsl:attribute name="type">
                            <xsl:value-of select="$type"/>
                        </xsl:attribute>
                        <xsl:copy-of select="exsl:node-set($notes)/tei:note[@xml:id = $id]"
                            xmlns:exsl="http://exslt.org/common"/>
                    </xsl:element>
                </xsl:for-each>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="withoutDuplicate_list">
            <xsl:for-each select="exsl:node-set($all_list)/*" xmlns:exsl="http://exslt.org/common">
                <xsl:variable name="this_name" select="name()"/>
                <xsl:variable name="this_position" select="position()"/>
                <xsl:if test="count(preceding-sibling::*[name() = $this_name]) = 0">
                    <xsl:element name="{$this_name}">
                        <xsl:attribute name="type">
                            <xsl:value-of select="@type"/>
                        </xsl:attribute>
                        <xsl:copy-of select="./tei:note"/>
                    </xsl:element>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:for-each select="exsl:node-set($withoutDuplicate_list)/*"
            xmlns:exsl="http://exslt.org/common">
            <xsl:variable name="this_name" select="name()"/>
            <xsl:variable name="this_position" select="position()"/>
            <xsl:element name="{name()}">
                <xsl:attribute name="type">
                    <xsl:value-of select="@type"/>
                </xsl:attribute>
                <xsl:attribute name="num">
                    <xsl:value-of select="count(preceding-sibling::*[@type = 'ann']) + 1"/>
                </xsl:attribute>
                <xsl:copy-of select="./tei:note"/>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>
</xsl:stylesheet>
