<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mon-document [
    <!ENTITY times "&#215;">
    <!ENTITY non_breakable_space "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="2.0">
    <!--<xsl:param name="ask">linearized</xsl:param>-->
    <xsl:param name="ask">pseudodiplo</xsl:param>
    <xsl:param name="level">page</xsl:param>
    <xsl:param name="pagenum">4v</xsl:param>
    <xsl:param name="iiif_thumbnail_option">/full/158,/0/native.jpg</xsl:param>
    <!--<xsl:param name="notenum">2</xsl:param>-->
    <xsl:include href="common.xsl"/>
    <xsl:include href="tei2pseudodiplo.xsl"/>
    <xsl:include href="tei2linearized.xsl"/>
    <xsl:include href="tei2criticalnotes.xsl"/>
    <xsl:output method="html" indent="no" omit-xml-declaration="yes"/>
    <!--<xsl:strip-space elements="*"/>
    <xsl:strip-space elements="tei:*"/>-->
    <xsl:preserve-space elements="tei:placeName"/>


    <xsl:param name="current_mode">
        <xsl:value-of select="$level"/>
        <xsl:text>-</xsl:text>
        <xsl:value-of select="$ask"/>
    </xsl:param>

    <xsl:template match="/">
        <xsl:variable name="normalized_forms" as="node()">
            <xsl:variable name="collection" select="concat($xml_dir, '?select=*.xml')"/>
            <xsl:variable name="tei_file" select="collection($collection)"/>
            <div xmlns="http://www.tei-c.org/ns/1.0">
                <xsl:copy-of select="$tei_file//*[@xml:id = 'formesnormalisées']"/>
            </div>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="$current_mode = 'cahier-id'">
                <xsl:apply-templates mode="cahier-id"/>
            </xsl:when>
            <xsl:when test="$current_mode = 'cahier-name'">
                <xsl:apply-templates mode="cahier-name"/>
            </xsl:when>
            <xsl:when test="$current_mode = 'cahier-dates'">
                <xsl:apply-templates mode="cahier-dates"/>
            </xsl:when>
            <xsl:when test="$current_mode = 'cahier-edito'">
                <xsl:apply-templates mode="cahier-edito"/>
            </xsl:when>
            <xsl:when test="$current_mode = 'cahier-previous'"> NA
                <!-- Will be available when a TEI Corpus will exists... -->
            </xsl:when>
            <xsl:when test="$current_mode = 'cahier-next'"> NA
                <!-- Will be available when a TEI Corpus will exists... -->
            </xsl:when>
            <xsl:when test="$current_mode = 'cahier-cahier'"> NA <!-- Non sense -->
            </xsl:when>
            <xsl:when test="$current_mode = 'cahier-image'"> NA <!-- Non sense -->
            </xsl:when>
            <xsl:when test="$current_mode = 'page-id'">
                <xsl:apply-templates mode="page-id"/>
            </xsl:when>
            <xsl:when test="$current_mode = 'page-previous'">
                <xsl:apply-templates mode="page-previous"/>
            </xsl:when>
            <xsl:when test="$current_mode = 'page-next'">
                <xsl:apply-templates mode="page-next"/>
            </xsl:when>
            <xsl:when test="$current_mode = 'page-cahier'">
                <xsl:apply-templates mode="page-cahier"/>
            </xsl:when>
            <xsl:when test="$current_mode = 'page-image'">
                <xsl:apply-templates mode="page-image"/>
            </xsl:when>
            <xsl:when test="$current_mode = 'page-thumbnail'">
                <xsl:apply-templates mode="page-thumbnail"/>
            </xsl:when>
            <xsl:when test="$current_mode = 'page-content'">
                <xsl:apply-templates mode="page-content"/>
            </xsl:when>
            <xsl:when test="$current_mode = 'page-pseudodiplo'">
                <xsl:apply-templates mode="page-pseudodiplo">
                    <xsl:with-param name="normalized_forms" select="$normalized_forms" as="node()" tunnel="yes"/>
                </xsl:apply-templates>
            </xsl:when>
            <xsl:when test="$current_mode = 'page-linearized'">
                <xsl:apply-templates mode="page-linearized">
                    <xsl:with-param name="normalized_forms" select="$normalized_forms" as="node()" tunnel="yes"/>
                </xsl:apply-templates>
            </xsl:when>
            <xsl:when test="$current_mode = 'page-notes'">
                <xsl:apply-templates mode="page-notes">
                    <xsl:with-param name="normalized_forms" select="$normalized_forms" as="node()" tunnel="yes"/>
                </xsl:apply-templates>
            </xsl:when>
            <xsl:otherwise> TODO </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="/tei:TEI" mode="cahier-id">
        <xsl:value-of
            select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/tei:name[@type = 'cahier']/@xml:id"
        />
    </xsl:template>

    <xsl:template match="/tei:TEI" mode="cahier-name">
        <xsl:value-of
            select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/tei:name[@type = 'cahier']"/>
    </xsl:template>

    <xsl:template match="/tei:TEI" mode="cahier-dates">
        <xsl:value-of
            select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title/tei:date[@type = 'cahier']"/>
    </xsl:template>

    <xsl:template match="/tei:TEI" mode="cahier-edito">
        <xsl:choose>
            <xsl:when test="tei:teiHeader/tei:profileDesc/tei:abstract">
                <xsl:apply-templates select="tei:teiHeader/tei:profileDesc/tei:abstract"
                    mode="edito"/>
                <!-- TODO : Finir dès que des contenus seront disponibles -->
            </xsl:when>
            <xsl:otherwise>
                <p>Présentation à venir.</p>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="tei:p" mode="edito">
        <p class="text-justify">
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="tei:title" mode="edito">
        <i>
            <xsl:apply-templates/>
        </i>
    </xsl:template>
    <xsl:template match="/tei:TEI" mode="page-id">
        <xsl:value-of
            select="substring-after(//tei:fw[text() = $pagenum and @type = 'num_page_ref']/preceding::tei:pb[1]/@facs, '#')"
        />
    </xsl:template>

    <xsl:template match="/tei:TEI" mode="page-previous">
        <xsl:choose>
            <xsl:when
                test="count(//tei:fw[text() = $pagenum and @type = 'num_page_ref']/preceding::tei:fw[@type = 'num_page_ref']) > 0">
                <xsl:value-of
                    select="//tei:fw[text() = $pagenum and @type = 'num_page_ref']/preceding::tei:fw[@type = 'num_page_ref'][1]"
                />
            </xsl:when>
            <xsl:otherwise> None </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="/tei:TEI" mode="page-next">
        <xsl:choose>
            <xsl:when
                test="count(//tei:fw[text() = $pagenum and @type = 'num_page_ref']/following::tei:fw[@type = 'num_page_ref']) > 0">
                <xsl:value-of
                    select="//tei:fw[text() = $pagenum and @type = 'num_page_ref']/following::tei:fw[@type = 'num_page_ref'][1]"
                />
            </xsl:when>
            <xsl:otherwise> None </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="/tei:TEI" mode="page-cahier">
        <xsl:apply-templates mode="cahier-id"
            select="//tei:fw[text() = $pagenum and @type = 'num_page_ref']/ancestor::tei:TEI[1]"/>
    </xsl:template>

    <xsl:template match="/tei:TEI" mode="page-image">
        <xsl:variable name="facs">
            <xsl:value-of
                select="substring-after(//tei:fw[text() = $pagenum and @type = 'num_page_ref']/preceding::tei:pb[1]/@facs, '#')"
            />
        </xsl:variable>
        <xsl:value-of select="normalize-space(//tei:graphic[@xml:id = $facs]/@url)"/>
    </xsl:template>

    <xsl:template match="/tei:TEI" mode="page-thumbnail">
        <xsl:variable name="facs">
            <xsl:value-of
                select="substring-after(//tei:fw[text() = $pagenum and @type = 'num_page_ref']/preceding::tei:pb[1]/@facs, '#')"
            />
        </xsl:variable>
        <!-- TODO: Improve in case source is not on Gallica... Normaly will be the case for the Bibliothèque de l'Institut images -->
        <xsl:text>https://gallica.bnf.fr/iiif/</xsl:text>
        <xsl:value-of
            select="substring-after(substring-before(normalize-space(//tei:graphic[@xml:id = $facs]/@url), '.highres'), 'gallica.bnf.fr/')"/>
        <xsl:value-of select="$iiif_thumbnail_option"/>
    </xsl:template>

    <xsl:template match="/tei:TEI" mode="page-content">
        <xsl:copy-of
            select="//tei:fw[text() = $pagenum and @type = 'num_page_ref']/ancestor::tei:div[@type = 'page']"/>

    </xsl:template>

    <xsl:template match="/tei:TEI" mode="page-pseudodiplo">
        <xsl:apply-templates
            select="//tei:fw[text() = $pagenum and @type = 'num_page_ref']/ancestor::tei:div[@type = 'page']"
            mode="pseudodiplo"/>
    </xsl:template>

    <xsl:template match="/tei:TEI" mode="page-linearized">
        <xsl:apply-templates
            select="//tei:fw[text() = $pagenum and @type = 'num_page_ref']/ancestor::tei:div[@type = 'page']"
            mode="linearized"/>
    </xsl:template>

    <xsl:template match="/tei:TEI" mode="page-notes">
        <dl class="apparatus">
            <!--<xsl:apply-templates select="//tei:fw[text() = $pagenum and @type = 'num_page_ref']/ancestor::tei:div[@type='page']" mode="apparatus"/>-->
            <xsl:apply-templates
                select="tei:text/tei:body//tei:note[ancestor::tei:div[@type = 'page']//tei:fw = $pagenum]"
                mode="notes"/>
        </dl>
    </xsl:template>
</xsl:stylesheet>
