<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mon-document [
    <!ENTITY times "&#215;">
    <!ENTITY non_breakable_space "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:str="http://exslt.org/strings" exclude-result-prefixes="xs tei" version="1.0">
    <xsl:import href="str.tokenize.template.xsl"/>
    
    <!--<xsl:output method="html" omit-xml-declaration="yes" indent="no"/>-->
    <!--<xsl:strip-space elements="*"/>-->

    <xsl:template match="/tei:TEI" mode="linearized">
        <xsl:apply-templates mode="linearized"/>
    </xsl:template>


    <xsl:template match="tei:teiHeader" mode="linearized"/>
    <xsl:template match="tei:facsimile" mode="linearized"/>
    <xsl:template match="tei:pb" mode="linearized"/>

    <xsl:template match="tei:text" mode="linearized">
        <div class="row">
            <div class="col col-12 text-justify">
                <xsl:apply-templates mode="linearized"/>
            </div>
        </div>
    </xsl:template>

    <xsl:template match="tei:front/tei:titlePage" mode="linearized">
        <div class="front">
            <xsl:apply-templates mode="linearized"/>
        </div>
    </xsl:template>

    <xsl:template match="tei:note" mode="linearized">
        <sup class="note-call" aria-hidden="true">
            <xsl:attribute name="id">
                <xsl:text>#a</xsl:text>
                <xsl:number count="tei:note" level="any"/>
            </xsl:attribute>
            <xsl:attribute name="data-note-id">
                <xsl:text>#n</xsl:text>
                <xsl:number count="tei:note" level="any"/>
            </xsl:attribute>
            <xsl:number count="tei:note" level="any"/>
        </sup>
    </xsl:template>

    <xsl:template match="tei:note" mode="apparatus">
        <div class="note">
            <xsl:apply-templates mode="linearized"/>
        </div>
    </xsl:template>


    <xsl:template match="tei:fw" mode="linearized">
        <!--<fw type="num_page_ref" hand="bib" place="top-right">18r</fw>-->
        <mark>
            <xsl:attribute name="class">
                <xsl:value-of select="@type"/>
                <xsl:text> semantic reset-styles</xsl:text>
                <xsl:if test="@place = 'top_right'"> text_right </xsl:if>
            </xsl:attribute>
            <xsl:attribute name="title">
                <xsl:text>Entête de type : '</xsl:text>
                <xsl:value-of select="@type"/>
                <xsl:text>' dont l'auteur est '</xsl:text>
                <xsl:value-of select="@hand"/>
                <xsl:text>'.</xsl:text>
            </xsl:attribute>
            <xsl:apply-templates mode="linearized"/>
        </mark>
        <br/>
    </xsl:template>

    <xsl:template match="tei:titlePart" mode="linearized">
        <h3 class="titlePart">
            <xsl:apply-templates mode="linearized"/>
        </h3>
    </xsl:template>

    <xsl:template match="tei:byline" mode="todo">
        <span>
            <xsl:apply-templates mode="todo"/>
        </span>
    </xsl:template>

    <xsl:template match="tei:div[@type = 'note']" mode="linearized">
        <div>
            <xsl:attribute name="class">
                <xsl:text>hr_note</xsl:text>
            </xsl:attribute>
            <xsl:apply-templates mode="linearized"/>
        </div>
    </xsl:template>

    <xsl:template match="tei:p" mode="linearized">
        <p>
            <xsl:apply-templates mode="linearized"/>
        </p>
    </xsl:template>

    <xsl:template match="tei:subst" mode="linearized">
        <span class="subst">
            <xsl:apply-templates mode="linearized"/>
        </span>
    </xsl:template>

    <xsl:template match="tei:del" mode="linearized"/>

    <xsl:template match="tei:gap" mode="linearized">
        <mark class="gap reading reset-styles">
            <xsl:attribute name="title">
                <xsl:text>Zone non transcrite car </xsl:text><xsl:value-of select="@reason"/>
            </xsl:attribute>
            <xsl:attribute name="style">
                <xsl:text>width: </xsl:text>
                <xsl:variable name="size_gap">
                    <xsl:choose>
                        <xsl:when test="number(substring-before(@extent, ' '))">
                            <xsl:value-of select="number(substring-before(@extent, ' '))"/>
                        </xsl:when>
                        <xsl:otherwise>2</xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:value-of select="$size_gap"/>
                <xsl:choose>
                    <xsl:when
                        test="substring-after(@extent, ' ') = 'lettres' or substring-after(@extent, ' ') = 'lettre'">
                        <xsl:text>ex</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>em</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            ????????????????????????????????????????????????????????????????????????? </mark>
    </xsl:template>

    <xsl:template match="tei:add" mode="linearized">
        <span class="add">
            <xsl:attribute name="class"/>
            <xsl:apply-templates mode="linearized"/>
        </span>
    </xsl:template>

    <xsl:template match="tei:choice" mode="linearized">
        <xsl:choose>
            <xsl:when test="./tei:sic or ./tei:corr">
                <mark class="content reset-styles">
                    <xsl:attribute name="title">
                        <xsl:text>Texte initial : '</xsl:text>
                        <xsl:value-of select="tei:sic"/>
                        <xsl:text>'</xsl:text>
                    </xsl:attribute>
                    <xsl:choose>
                        <xsl:when test="tei:corr = ''">
                            <xsl:text> </xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:apply-templates select="tei:corr" mode="linearized"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </mark>
            </xsl:when>
            <xsl:when test="count(./tei:unclear) = count(./*)">
                <xsl:variable name="max_unclear_level">
                    <xsl:choose>
                        <xsl:when test="count(./tei:unclear[@cert = 'high']) = 1">
                            <xsl:text>high</xsl:text>
                        </xsl:when>
                        <xsl:when test="count(./tei:unclear[@cert = 'medium']) = 1">
                            <xsl:text>medium</xsl:text>
                        </xsl:when>
                        <xsl:when test="count(./tei:unclear[@cert = 'low']) = 1">
                            <xsl:text>low</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>unknown</xsl:text>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <mark class="reading reset-styles">
                    <xsl:attribute name="title">
                        <xsl:variable name="plural_mark">
                            <xsl:if test="count(./tei:unclear) > 2">
                                <xsl:text>s</xsl:text>
                            </xsl:if>
                        </xsl:variable>
                        <xsl:text>Autre</xsl:text>
                        <xsl:value-of select="$plural_mark"/>
                        <xsl:text> lecture</xsl:text>
                        <xsl:value-of select="$plural_mark"/>
                        <xsl:text> possible</xsl:text>
                        <xsl:value-of select="$plural_mark"/>
                        <xsl:text> : </xsl:text>
                        <xsl:for-each select="tei:unclear[@cert != $max_unclear_level]">
                            <xsl:if test="position() > 1 and position() != last()">
                                <xsl:text>, </xsl:text>
                                <xsl:value-of select="."/>
                            </xsl:if>
                            <xsl:if test="position() = last() and position() != 1">
                                <xsl:text>ou </xsl:text>
                                <xsl:value-of select="."/>
                            </xsl:if>
                            <xsl:if test="position() = last() and position() = 1">
                                <xsl:value-of select="."/>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:attribute>
                    <xsl:value-of select="./tei:unclear[@cert = $max_unclear_level]"/>
                </mark>
            </xsl:when>
            <xsl:when test="./tei:abbr or ./tei:expan">
                <mark class="enrichment reset-styles" title="Abréviation">
                    <xsl:attribute name="title">
                        <xsl:value-of select="normalize-space(tei:abbr)"/>
                    </xsl:attribute>
                    <xsl:value-of select="normalize-space(tei:expan)"/>
                </mark>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>(</xsl:text>
                <xsl:for-each select="*">
                    <xsl:choose>
                        <xsl:when test="position() = 1">
                            <xsl:apply-templates mode="linearized"/>
                        </xsl:when>
                        <xsl:when test="position() = last()">
                            <xsl:text> ou </xsl:text>
                            <xsl:apply-templates mode="linearized"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:text>, </xsl:text>
                            <xsl:apply-templates mode="linearized"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:for-each>
                <xsl:text>)</xsl:text>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="tei:corr" mode="linearized">
        <mark class="corr content reset-styles">
            <xsl:apply-templates mode="linearized"/>
        </mark>
    </xsl:template>

    <xsl:template match="tei:sic" mode="linearized"/>

    <!--<metamark function="separator" place="inline" rend="horizontal_line"/>-->
    <xsl:template match="tei:metamark" mode="linearized">
        <xsl:apply-templates mode="linearized"/>
    </xsl:template>
    <!--<xsl:template match="tei:metamark" mode="linearized">
        <span>
            <xsl:attribute name="class">
                <xsl:text>metamark </xsl:text>
                <xsl:text>function_</xsl:text>
                <xsl:value-of select="@function"/>
                <xsl:text> </xsl:text>
                <xsl:choose>
                    <xsl:when test="@place = 'inline'">
                        <xsl:choose>
                            <xsl:when test="@rend = 'horizontal_line'">
                                <xsl:text> border_top</xsl:text>
                            </xsl:when>
                            <xsl:when test="@rend = 'blank'">
                                <xsl:text> blank_space</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:text>rend_</xsl:text>
                                <xsl:value-of select="@rend"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:when test="@place = 'margin_l'">
                        <xsl:text> border_left</xsl:text>
                    </xsl:when>
                    <xsl:when test="@place = 'margin_r'">
                        <xsl:text> border_right</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>place_</xsl:text>
                        <xsl:value-of select="@place"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:attribute>
            <xsl:apply-templates mode="linearized"/>
        </span>
    </xsl:template>-->

    <xsl:template match="tei:placeName" mode="linearized">
        <mark class="enrichment reset-styles" title="Nom de lieu">
            <xsl:apply-templates mode="linearized"/>
        </mark>
    </xsl:template>

    <xsl:template match="tei:ref" mode="linearized">
        <xsl:variable name="note_list">
            <xsl:call-template name="note_list_linearized"/>
        </xsl:variable>
        <xsl:variable name="sep">
            <xsl:text> </xsl:text>
        </xsl:variable>
        <xsl:variable name="ids">
            <xsl:call-template name="str:tokenize">
                <xsl:with-param name="string" select="@target"/>
                <xsl:with-param name="delimiters" select="$sep"/>
            </xsl:call-template>
        </xsl:variable>
        <xsl:variable name="notes">
            <xsl:copy-of select="//tei:note"/>
        </xsl:variable>
        <xsl:apply-templates mode="linearized"/>
        <xsl:for-each select="exsl:node-set($ids)/token" xmlns:exsl="http://exslt.org/common">
            <xsl:variable name="note_id">
                <xsl:value-of select="substring-after(., '#')"/>
            </xsl:variable>
            <xsl:variable name="note_type">
                <xsl:value-of select="exsl:node-set($note_list)/*[name() = $note_id]/@type"
                    xmlns:exsl="http://exslt.org/common"/>
            </xsl:variable>
            <xsl:variable name="note_num">
                <xsl:value-of select="exsl:node-set($note_list)/*[name() = $note_id]/@num"
                    xmlns:exsl="http://exslt.org/common"/>
            </xsl:variable>
            <xsl:choose>
                <xsl:when test="$note_type = 'ann'">
                    <sup class="note-call badge">
                        <xsl:attribute name="data-note-id">
                            <xsl:value-of select="$note_id"/>
                        </xsl:attribute>
                        <xsl:attribute name="data-type">
                            <xsl:value-of select="$note_type"/>
                        </xsl:attribute>
                        <xsl:value-of select="$note_num"/>
                    </sup>
                </xsl:when>
                <xsl:when test="$note_type = 'not'">
                    <sup class="note-call badge">
                        <xsl:attribute name="data-note-id">
                            <xsl:value-of select="$note_id"/>
                        </xsl:attribute>
                        <xsl:attribute name="data-type">
                            <xsl:value-of select="$note_type"/>
                        </xsl:attribute>
                        <i class="fa fa-info-circle"/>
                    </sup>
                </xsl:when>
                <xsl:otherwise>
                    <sup class="note-call badge">
                        <xsl:attribute name="data-note-id">
                            <xsl:value-of select="$note_id"/>
                        </xsl:attribute>
                        <xsl:attribute name="data-type">
                            <xsl:value-of select="$note_type"/>
                        </xsl:attribute>
                        <i class="fa fa-todo"
                            title="Type de note non déclaré. Renseigner l'attribut @ana de la note @xml:id='{$note_id}'"
                        />
                    </sup>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="tei:hi[@rend = 'underline']" mode="linearized">
        <i>
            <xsl:apply-templates mode="linearized"/>
        </i>
    </xsl:template>

    <xsl:template match="tei:hi[@rend = 'exp']" mode="linearized">
        <sup>
            <xsl:apply-templates mode="linearized"/>
        </sup>
    </xsl:template>

    <xsl:template match="tei:title" mode="linearized">
        <i>
            <xsl:apply-templates mode="linearized"/>
        </i>
    </xsl:template>

    <xsl:template match="tei:date" mode="linearized">
        <mark class="enrichment reset-styles">
            <xsl:attribute name="title"> Date : <xsl:value-of
                    select="concat(substring(@when, 9, 4), '/', substring(@when, 6, 2), '/', substring(@when, 1, 4))"
                />
            </xsl:attribute>
            <xsl:attribute name="date-searchable">
                <xsl:value-of select="@when"/>
            </xsl:attribute>
            <xsl:apply-templates mode="linearized"/>
        </mark>
    </xsl:template>

    <xsl:template match="tei:unclear" mode="linearized">
        <mark>
            <xsl:attribute name="class">
                <xsl:text>reset-styles unclear reading</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="title">
                <xsl:choose>
                    <xsl:when test="@cert = 'low'">
                        <xsl:text>Lecture incertaine</xsl:text>
                    </xsl:when>
                    <xsl:when test="@cert = 'medium'">
                        <xsl:text>Lecture possible</xsl:text>
                    </xsl:when>
                    <xsl:when test="@cert = 'high'">
                        <xsl:text>Lecture probable</xsl:text>
                    </xsl:when>
                    <xsl:when test="string-length(@cert) = 0">
                        <xsl:text>Attention balise &lt;unclear/> : valeur vide pour l'attribut @cert </xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>Attention balise &lt;unclear/> : valeur inattendue de l'attribut @cert "</xsl:text>
                        <xsl:value-of select="@cert"/>
                        <xsl:text>"</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
                <!--<xsl:text> car </xsl:text>
                <xsl:choose>
                    <xsl:when test="@reason = 'illegible'">
                        illisible
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="@reason"/>
                    </xsl:otherwise>
                </xsl:choose>-->
            </xsl:attribute>
            <xsl:apply-templates mode="linearized"/>
        </mark>
    </xsl:template>

    <xsl:template match="tei:abbr" mode="linearized">
        <mark class="enrichment reset-styles" title="Abréviation">
            <xsl:attribute name="title">
                <xsl:text>Texte original : "</xsl:text>
                <xsl:value-of select="normalize-space(tei:sic)"/>
                <xsl:text>"</xsl:text>
            </xsl:attribute>
            <xsl:apply-templates select="tei:corr" mode="linearized"/>
        </mark>
    </xsl:template>

    <xsl:template match="tei:foreign" mode="linearized">
        <mark class="enrichment reset-styles" title="Emprunt {@xml:lang}">
            <i>
                <xsl:apply-templates mode="linearized"/>
            </i>
        </mark>
    </xsl:template>

    <xsl:template match="tei:lb" mode="linearized"/>

    <!-- Copy and adaptation of template note_list from tei2criticalnotes.xsl -->
    <xsl:template name="note_list_linearized">
        <xsl:variable name="all_list">
            <xsl:for-each select="ancestor::tei:div[@type = 'page']//tei:ref">
                <!-- to adapt from template note_list from tei2criticalnotes.xsl-->
                <xsl:variable name="sep">
                    <xsl:text> </xsl:text>
                </xsl:variable>
                <xsl:variable name="ids">
                    <xsl:call-template name="str:tokenize">
                        <xsl:with-param name="string" select="@target"/>
                        <xsl:with-param name="delimiters" select="$sep"/>
                    </xsl:call-template>
                </xsl:variable>
                <xsl:variable name="notes">
                    <xsl:copy-of select="//tei:note"/>
                </xsl:variable>
                <xsl:for-each select="exsl:node-set($ids)/token"
                    xmlns:exsl="http://exslt.org/common">
                    <xsl:variable name="id">
                        <xsl:value-of select="substring-after(., '#')"/>
                    </xsl:variable>
                    <!--<xsl:message>[<xsl:value-of select="count($notes/tei:note[@xml:id = 'HR_1887-02_PL_72'])"/>]
                    [<xsl:value-of select="$id"/>]</xsl:message>-->
                    <xsl:variable name="type">
                        <xsl:choose>
                            <xsl:when
                                test="count(exsl:node-set($notes)/tei:note[@xml:id = $id]) = 0"
                                xmlns:exsl="http://exslt.org/common">
                                <xsl:text>err:not_available</xsl:text>
                            </xsl:when>
                            <xsl:when
                                test="count(exsl:node-set($notes)/tei:note[@xml:id = $id]) > 1"
                                xmlns:exsl="http://exslt.org/common">
                                <xsl:text>err:more_than_one_target</xsl:text>
                            </xsl:when>
                            <xsl:when test="not(exsl:node-set($notes)/tei:note[@xml:id = $id]/@ana)"
                                xmlns:exsl="http://exslt.org/common">
                                <xsl:text>err:missing_ann_attribute</xsl:text>
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of
                                    select="substring(exsl:node-set($notes)/tei:note[@xml:id = $id]/@ana, 2, 3)"
                                    xmlns:exsl="http://exslt.org/common"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:variable>
                    <xsl:element name="{$id}">
                        <xsl:attribute name="type">
                            <xsl:value-of select="$type"/>
                        </xsl:attribute>
                        <xsl:copy-of select="exsl:node-set($notes)/tei:note[@xml:id = $id]"
                            xmlns:exsl="http://exslt.org/common"/>
                    </xsl:element>
                </xsl:for-each>
            </xsl:for-each>
        </xsl:variable>
        <xsl:variable name="withoutDuplicate_list">
            <xsl:for-each select="exsl:node-set($all_list)/*" xmlns:exsl="http://exslt.org/common">
                <xsl:variable name="this_name" select="name()"/>
                <xsl:variable name="this_position" select="position()"/>
                <xsl:if test="count(preceding-sibling::*[name() = $this_name]) = 0">
                    <xsl:element name="{$this_name}">
                        <xsl:attribute name="type">
                            <xsl:value-of select="@type"/>
                        </xsl:attribute>
                        <xsl:copy-of select="./tei:note"/>
                    </xsl:element>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:for-each select="exsl:node-set($withoutDuplicate_list)/*"
            xmlns:exsl="http://exslt.org/common">
            <xsl:variable name="this_name" select="name()"/>
            <xsl:variable name="this_position" select="position()"/>
            <xsl:element name="{name()}">
                <xsl:attribute name="type">
                    <xsl:value-of select="@type"/>
                </xsl:attribute>
                <xsl:attribute name="num">
                    <xsl:value-of select="count(preceding-sibling::*[@type = 'ann']) + 1"/>
                </xsl:attribute>
                <xsl:copy-of select="./tei:note"/>
            </xsl:element>
        </xsl:for-each>
    </xsl:template>


</xsl:stylesheet>
