$(function() {
  cookieHandler.check("left");
  cookieHandler.check("right");

  var inputs = document.querySelectorAll(".col-left .nav-link")
  for (i = 0; i < inputs.length; i++) {
    inputs[i].addEventListener('click', function() {
      let href = this.dataset.href
      cookieHandler.set("left-tab", href, 30)
    });
  }

  inputs = document.querySelectorAll(".col-right .nav-link")
  for (i = 0; i < inputs.length; i++) {
    inputs[i].addEventListener('click', function() {
      let href = this.dataset.href
      cookieHandler.set("right-tab", href, 30)
    });
  }
});

var cookieHandler = {
  set: function(name, value, daysToLive) {
    var cookie = name + "=" + encodeURIComponent(value);
    if (typeof daysToLive === "number") {
      cookie += "; max-age=" + (daysToLive * 24 * 60 * 60);
      document.cookie = cookie;
    }
  },

  check: function(name) {
    var cookie = cookieHandler.get(name + "-tab");
    if (cookie) {
      let selector = "[data-href='" + cookie + "']"
      let newLink = document.querySelector(selector);
      if (newLink) {
        document.querySelector(".col-" + name + " .nav-link.active").classList.remove("active");
        document.querySelector(".col-" + name + " .tab-pane.show.active").classList.remove("show", "active");
        newLink.classList.add("active");
        document.getElementById(cookie).classList.add("show", "active")
      }
    }
  },

  get: function(name) {
    var cookieArr = document.cookie.split(";");

    for (var i = 0; i < cookieArr.length; i++) {
      var cookiePair = cookieArr[i].split("=");
      if (name == cookiePair[0].trim()) {
        return decodeURIComponent(cookiePair[1]);
      }
    }

    return null;
  }

}
