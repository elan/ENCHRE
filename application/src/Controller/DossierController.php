<?php

namespace App\Controller;

use App\Manager\XmlManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DossierController extends AbstractController
{
    /**
     * @Route("/dossier/chronologie", name="chronologie")
     */
    public function chronologie()
    {
        return $this->render('dossier/chronologie.html.twig');
    }

    /**
     * @Route("/dossier/bibliographie", name="bibliographie")
     */
    public function bibliographie()
    {
        return $this->render('dossier/bibliographie.html.twig');
    }

    /**
     * @Route("/dossier/index-persons", name="index-persons")
     */
    public function indexPersons(XmlManager $xm)
    {
        $html = $xm->getIndexPersonsHTML();

        return $this->render('dossier/index-persons.html.twig', [
            "html" => $html
        ]);
    }
    /**
     * @Route("/dossier/index-works", name="index-works")
     */
    public function indexWorks(XmlManager $xm)
    {
        $html = $xm->getIndexWorksHTML();

        return $this->render('dossier/index-works.html.twig', [
            "html" => $html
        ]);
    }
    /**
     * @Route("/dossier/index-notions", name="index-notions")
     */
    public function indexNotions(XmlManager $xm)
    {
        $html = $xm->getIndexNotionsHTML();

        return $this->render('dossier/index-notions.html.twig', [
            "html" => $html
        ]);
    }

    /**
     * @Route("/dossier/table-des-cahiers", name="table_cahiers")
     */
    public function tableCahiers()
    {
        return $this->render('dossier/table-cahiers.html.twig');
    }

}
