<?php

namespace App\Controller;

use App\Manager\XmlManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class EditoController extends AbstractController
{
    /**
     * @Route("/encoding", name="encoding")
     */
    public function encoding()
    {
        return $this->render('edito/encoding.html.twig');
    }

    /**
     * @Route("/mode-emploi", name="mode_emploi")
     */
    public function modeEmploi()
    {
        return $this->render('edito/mode-emploi.html.twig');
    }

    /**
     * @Route("/presentation/corpus", name="edito_corpus")
     */
    public function corpus(XmlManager $xm)
    {
        $html = $xm->getEditoHTML("corpus");
    
        return $this->render('edito/corpus.html.twig', [
            "html" => $html
        ]);
    }

    /**
     * @Route("/presentation/principes-edition", name="edito_principes")
     */
    public function principesEdition(XmlManager $xm)
    {
        $html = $xm->getEditoHTML("principes-edition");
        
        return $this->render('edito/principes-edition.html.twig', [
            "html" => $html
        ]);
    }

    /**
     * @Route("/presentation/equipe", name="equipe_partenaires")
     */
    public function equipePartenaires(XmlManager $xm)
    {
        $html = $xm->getEditoHTML("equipe");

        return $this->render('edito/equipe-partenaires.html.twig', [
            "html" => $html
        ]);
    }
}
