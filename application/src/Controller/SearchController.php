<?php

namespace App\Controller;

use App\Form\TextSearchType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Cahier;
use App\Manager\XmlManager;

/**
 * @Route("/search", name="search_")
 */
class SearchController extends AbstractController
{
    /**
     * @Route("/index", name="index")
     */
    public function index(Request $request, XmlManager $xmlManager)
    {
        $form =  $this->createForm(TextSearchType::class, null);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $str = $form['request']->getData();
            $searchIn = $form->get('search_in')->getData();
            $caseSensitive = $form->get('case_sensitive')->getData();
            $cahiers = ($this->isGranted('ROLE_ADMIN'))
                ? $this->getDoctrine()->getRepository(Cahier::class)->findAll()
                : $this->getDoctrine()->getRepository(Cahier::class)->findByPublic(true);

            $out = [];
            $places = ["notes", "pseudodiplo", "linearized"];
            foreach ($cahiers as $cahier) {
                $cahierSlug = $cahier->getSlugName();
                $pages = $cahier->getPages();
                foreach ($pages as $page) {
                    $text = "";
                    foreach ($places as $place) {
                        if(in_array($place, $searchIn)){
                            $text .= $xmlManager->getHTML($place, $cahier, "page", $page->getName());
                        }
                    }
                   
                    $text = strip_tags($text);
                    if(!$caseSensitive) {
                        $text = mb_strtolower($text);
                        $str = mb_strtolower($str);
                    }

                    $contains = str_contains($text, $str);

                    if($contains) {
                        if (!array_key_exists($cahierSlug, $out)) {
                            $out[$cahierSlug]["cahier"] = $cahier;
                            $out[$cahierSlug]["pages"] = [];
                        }
                        $out[$cahierSlug]["pages"][] = $page->getName();
                    }
                }
            }

            return $this->render('search/results.html.twig', ['request' => $str, 'out' => $out]);
        }

        return $this->render('search/index.html.twig', ['form' => $form->createView()]);
    }
}
