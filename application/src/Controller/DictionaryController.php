<?php

namespace App\Controller;

use App\Manager\XmlManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class DictionaryController extends AbstractController
{
    /**
     * @Route("/dictionnaire", name="dictionary")
     */
    public function dictionary(XmlManager $xm)
    {
        $html = $xm->getDictionnaryHTML();

        return $this->render('dictionary/index.html.twig', [
            "html" => $html
        ]);
    }
}
