<?php

namespace App\Command;

use App\Manager\XmlManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CacheWarmupCommand extends Command
{
    private $xm;
    protected static $defaultName = 'app:cache:warmup';

    public function __construct(XmlManager $xm)
    {
        $this->xm = $xm;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Warm up cache');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $this->xm->cacheWarmup();
        $io->success('Cache alimenté');
    }
}
