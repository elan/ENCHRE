<?php

namespace App\Manager;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Entity\Cahier;
use Saxon\SaxonProcessor;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Doctrine\ORM\EntityManagerInterface;

class XmlManager
{
    private $dir;
    private $params;
    private $uploadDir;
    private $cacheDir;
    private $em;

    public function __construct(ParameterBagInterface $params, EntityManagerInterface $em)
    {
        $this->params = $params;
        $this->uploadDir = $this->params->get('upload_directory');
        $this->cacheDir = $this->params->get('cache_directory');
        $this->dir = $this->params->get('kerneldir') . DIRECTORY_SEPARATOR . "data" . DIRECTORY_SEPARATOR . "xslt" . DIRECTORY_SEPARATOR;
        $this->em = $em;
    }

    public function getHTML($what, Cahier $cahier, $level, $pageName = null)
    {
        $filesystem = new Filesystem();
        $path = $this->cacheDir . 'cahier-' . $cahier->getSlugName() . ' -what-' . $what . '-level-' . $level . '-pageName-' . ($pageName ? $pageName : null) . '.html';
        if ($filesystem->exists($path)) {
            $html = file_get_contents($path);
            return $html;
        }

        $saxonProc = new SaxonProcessor();
        $xsltProc = $saxonProc->newXsltProcessor();
        $xmlFile = $this->uploadDir . 'transcriptions' . DIRECTORY_SEPARATOR . $cahier->getXmlFileName();
        $xsltProc->setSourceFromFile($xmlFile);
        $xslFile = $this->dir . 'giveFromTeiSplitedByPages.xsl';
        $xsltProc->compileFromFile($xslFile);
        if ($pageName) {
            $xsltProc->setParameter('pagenum', $saxonProc->createAtomicValue($pageName));
        }
        $xsltProc->setParameter('level', $saxonProc->createAtomicValue($level));
        $xsltProc->setParameter('ask', $saxonProc->createAtomicValue($what));
        $xml = $xsltProc->transformToString();

        $xsltProc->clearParameters();
        $xsltProc->clearProperties();
        unset($xsltProc);
        unset($saxonProc);

        $filesystem->dumpFile($path, $xml);

        return $xml;
    }

    public function getEditoHTML($what)
    {
        $filesystem = new Filesystem();
        $path = $this->cacheDir . $what . '.html';
        if ($filesystem->exists($path)) {
            $html = file_get_contents($path);
            return $html;
        }

        $xslFile = $this->dir . 'tei2editorial.xsl';
        $xmlFile = $this->uploadDir . 'common/template.xml';

        $saxonProc = new SaxonProcessor();
        $xsltProc = $saxonProc->newXsltProcessor();
        $xsltProc->setSourceFromFile($xmlFile);
        $xsltProc->compileFromFile($xslFile);
        $xsltProc->setParameter('slug', $saxonProc->createAtomicValue($what));
        $xml = $xsltProc->transformToString();

        $xsltProc->clearParameters();
        $xsltProc->clearProperties();
        unset($xsltProc);
        unset($saxonProc);

        $filesystem->dumpFile($path, $xml);

        return $xml;
    }

    public function getDictionnaryHTML()
    {
        $filesystem = new Filesystem();
        $path = $this->cacheDir . 'dictionnary.html';
        if ($filesystem->exists($path)) {
            $html = file_get_contents($path);
            return $html;
        }

        $xslFile = $this->dir . 'tei2dictionnary.xsl';
        $xmlFile = $this->uploadDir . 'common/template.xml';

        $saxonProc = new SaxonProcessor();
        $xsltProc = $saxonProc->newXsltProcessor();
        $xsltProc->setSourceFromFile($xmlFile);
        $xsltProc->compileFromFile($xslFile);
        $xml = $xsltProc->transformToString();

        $xsltProc->clearParameters();
        $xsltProc->clearProperties();
        unset($xsltProc);
        unset($saxonProc);

        $filesystem->dumpFile($path, $xml);

        return $xml;
    }

    public function getIndexWorksHTML()
    {
        $filesystem = new Filesystem();
        $path = $this->cacheDir . 'index-work.html';
        if ($filesystem->exists($path)) {
            $html = file_get_contents($path);
            return $html;
        }

        $xslFile = $this->dir . 'tei2index.xsl';
        $xmlFile = $this->uploadDir . 'common/template.xml';

        $saxonProc = new SaxonProcessor();
        $xsltProc = $saxonProc->newXsltProcessor();
        $xsltProc->setSourceFromFile($xmlFile);
        $xsltProc->compileFromFile($xslFile);
        $xsltProc->setParameter('index_type', $saxonProc->createAtomicValue('works'));
        $xml = $xsltProc->transformToString();

        $xsltProc->clearParameters();
        $xsltProc->clearProperties();
        unset($xsltProc);
        unset($saxonProc);

        $filesystem->dumpFile($path, $xml);

        return $xml;
    }

    public function getIndexPersonsHTML()
    {
        $filesystem = new Filesystem();
        $path = $this->cacheDir . 'index-persons.html';
        if ($filesystem->exists($path)) {
            $html = file_get_contents($path);
            return $html;
        }

        $xslFile = $this->dir . 'tei2index.xsl';
        $xmlFile = $this->uploadDir . 'common/template.xml';

        $saxonProc = new SaxonProcessor();
        $xsltProc = $saxonProc->newXsltProcessor();
        $xsltProc->setSourceFromFile($xmlFile);
        $xsltProc->compileFromFile($xslFile);
        $xsltProc->setParameter('index_type', $saxonProc->createAtomicValue('persons'));
        $xml = $xsltProc->transformToString();

        $xsltProc->clearParameters();
        $xsltProc->clearProperties();
        unset($xsltProc);
        unset($saxonProc);

        $filesystem->dumpFile($path, $xml);

        return $xml;
    }

    public function getIndexNotionsHTML()
    {
        $filesystem = new Filesystem();
        $path = $this->cacheDir . 'index-notions.html';
        if ($filesystem->exists($path)) {
            $html = file_get_contents($path);
            return $html;
        }

        $xslFile = $this->dir . 'tei2index.xsl';
        $xmlFile = $this->uploadDir . 'common/template.xml';

        $saxonProc = new SaxonProcessor();
        $xsltProc = $saxonProc->newXsltProcessor();
        $xsltProc->setSourceFromFile($xmlFile);
        $xsltProc->compileFromFile($xslFile);
        $xsltProc->setParameter('index_type', $saxonProc->createAtomicValue('notions'));
        $xml = $xsltProc->transformToString();

        $xsltProc->clearParameters();
        $xsltProc->clearProperties();
        unset($xsltProc);
        unset($saxonProc);

        $filesystem->dumpFile($path, $xml);

        return $xml;
    }

    public function cacheInvalidate()
    {
        $filesystem = new Filesystem();
        $finder = new Finder;
        $finder->in($this->cacheDir)->files()->name('*.html');

        foreach ($finder as $file) {
            $filesystem->remove($file->getRealPath());
        }

        return;
    }

    public function cacheWarmup()
    {
        $cahiers = $this->em->getRepository(Cahier::class)->findAll();

        foreach ($cahiers as $cahier) {
            echo "\n" . $cahier->getName() . "\n";
            echo "...\n";
            $pages = $cahier->getPages();
            foreach ($pages as $page) {
                $this->getHTML("pseudodiplo", $cahier, "page", $page->getName());
                $this->getHTML("linearized", $cahier, "page", $page->getName());
                $this->getHTML("notes", $cahier, "page", $page->getName());
            }
            echo $cahier->getName() . " terminé\n\n\n\n";
        }

        return;
    }
}
